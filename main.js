import { products } from './products.js';

function filterProducts() {
    const category = document.getElementById('categorySelect').value;
    const searchQuery = document.getElementById('searchInput').value.toLowerCase();

    const filteredProducts = products.filter(product => {
        return (category === 'all' || product.category === category) &&
                (searchQuery === '' || product.name.toLowerCase().includes(searchQuery));
    });

    displayProducts(filteredProducts);
}

function displayProducts(products) {
    const productList = document.getElementById('productList');
    productList.innerHTML = '';

    products.forEach(product => {
        const productBox = document.createElement('div');
        productBox.classList.add('col-md-3');
        productBox.innerHTML = `
            <div class="product-box">
                <img src="path_to_logo.png" alt="Product Logo" class="product-logo">
                <h4>${product.name}</h4>
                <p>Description of ${product.name}.</p>
            </div>
        `;
        productList.appendChild(productBox);
    });
}

// Initial display of all products
displayProducts(products);

document.addEventListener('DOMContentLoaded', function () {
    window.addEventListener('scroll', function() {
        const navbar = document.querySelector('.navbar');
        if (window.scrollY > navbar.offsetHeight) {
            navbar.classList.add('sticky-nav');
        } else {
            navbar.classList.remove('sticky-nav');
        }
    });
});
