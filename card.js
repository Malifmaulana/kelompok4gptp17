document.addEventListener('DOMContentLoaded', function() {
    const teamMembers = document.querySelectorAll('.team-member');
  
    function isInViewport(element) {
      const rect = element.getBoundingClientRect();
      const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
      return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }
  
    function checkInView() {
      teamMembers.forEach(member => {
        if (isInViewport(member)) {
          member.classList.add('show');
        } else {
          member.classList.remove('show');
        }
      });
    }
  
    window.addEventListener('scroll', checkInView);
    window.addEventListener('resize', checkInView);
  
    checkInView();
  });