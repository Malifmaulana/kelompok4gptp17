const products = [
    { name: "Product 1", category: "category1" },
    { name: "Product 2", category: "category2" },
    // Add the rest of your products here
];

export { products };
